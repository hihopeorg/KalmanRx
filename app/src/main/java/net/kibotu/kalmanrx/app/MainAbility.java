package net.kibotu.kalmanrx.app;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.openharmony.MainThreadDisposable;
import net.kibotu.kalmanrx.KalmanRx;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.sensor.agent.CategoryMotionAgent;
import ohos.sensor.bean.CategoryMotion;
import ohos.sensor.data.CategoryMotionData;
import ohos.sensor.data.CategoryOrientationData;
import ohos.sensor.listener.ICategoryMotionDataCallback;

public class MainAbility extends Ability {

    private float radiansToDegrees = 180f / 3.1415927f;

    private LineGraphSeries<DataPoint> xSeriesRaw;
    private LineGraphSeries<DataPoint> ySeriesRaw;
    private LineGraphSeries<DataPoint> zSeriesRaw;

    private LineGraphSeries<DataPoint> xSeriesKalman;
    private LineGraphSeries<DataPoint> ySeriesKalman;
    private LineGraphSeries<DataPoint> zSeriesKalman;

    private LineGraphSeries<DataPoint> xSeriesLowPass;
    private LineGraphSeries<DataPoint> ySeriesLowPass;
    private LineGraphSeries<DataPoint> zSeriesLowPass;

    private Text xLabelRaw;
    private Text yLabelRaw;
    private Text zLabelRaw;

    private Text xLabelLowPass;
    private Text yLabelLowPass;
    private Text zLabelLowPass;

    private Text xLabelKalman;
    private Text yLabelKalman;
    private Text zLabelKalman;

    private GraphView graphRaw;
    private GraphView graphLowPass;
    private GraphView graphKalman;

    private double graph2LastXValueRaw = 0.0;
    private double graph2LastXValueLowPass = 0.0;
    private double graph2LastXValueKalman = 0.0;

    private ICategoryMotionDataCallback orientationDataCallback;
    private CategoryMotion orientationSensor;
    private final int maxDataPoints = 1000;
    private CategoryMotionAgent categoryMotionAgent;

    private final float[] rotationMatrix = new float[9];
    private final float[] orientation = new float[3];

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        graphRaw = (GraphView) findComponentById(ResourceTable.Id_graphRaw);
        graphLowPass = (GraphView) findComponentById(ResourceTable.Id_graphLowPass);
        graphKalman = (GraphView) findComponentById(ResourceTable.Id_graphKalman);

        xLabelRaw = (Text) findComponentById(ResourceTable.Id_xLabelRaw);
        yLabelRaw = (Text) findComponentById(ResourceTable.Id_yLabelRaw);
        zLabelRaw = (Text) findComponentById(ResourceTable.Id_zLabelRaw);

        xLabelLowPass = (Text) findComponentById(ResourceTable.Id_xLabelLowPass);
        yLabelLowPass = (Text) findComponentById(ResourceTable.Id_yLabelLowPass);
        zLabelLowPass = (Text) findComponentById(ResourceTable.Id_zLabelLowPass);

        xLabelKalman = (Text) findComponentById(ResourceTable.Id_xLabelKalman);
        yLabelKalman = (Text) findComponentById(ResourceTable.Id_yLabelKalman);
        zLabelKalman = (Text) findComponentById(ResourceTable.Id_zLabelKalman);
    }

    @Override
    protected void onActive() {
        super.onActive();
        initRaw();
        initKalman();
        initLowPass();
        addToGraphRaw();
        addToGraphKalman();
        addToGraphLowPass();
    }

    private void initRaw() {
        xSeriesRaw = new LineGraphSeries<>();
        xSeriesRaw.setColor(new Color(Color.getIntColor("#FF0000")));
        ySeriesRaw = new LineGraphSeries<>();
        ySeriesRaw.setColor(new Color(Color.getIntColor("#00FF00")));
        zSeriesRaw = new LineGraphSeries<>();
        zSeriesRaw.setColor(new Color(Color.getIntColor("#0000FF")));
        graphRaw.getViewport().setXAxisBoundsManual(true);
        graphRaw.getViewport().setMinX(0.0);
        graphRaw.getViewport().setMaxX(maxDataPoints);
        graphRaw.addSeries(xSeriesRaw);
        graphRaw.addSeries(ySeriesRaw);
        graphRaw.addSeries(zSeriesRaw);
    }

    private void initKalman() {
        xSeriesKalman = new LineGraphSeries<>();
        xSeriesKalman.setColor(new Color(Color.getIntColor("#FF0000")));
        ySeriesKalman = new LineGraphSeries<>();
        ySeriesKalman.setColor(new Color(Color.getIntColor("#00FF00")));
        zSeriesKalman = new LineGraphSeries<>();
        zSeriesKalman.setColor(new Color(Color.getIntColor("#0000FF")));
        graphKalman.getViewport().setXAxisBoundsManual(true);
        graphKalman.getViewport().setMinX(0.0);
        graphKalman.getViewport().setMaxX(maxDataPoints);
        graphKalman.addSeries(xSeriesKalman);
        graphKalman.addSeries(ySeriesKalman);
        graphKalman.addSeries(zSeriesKalman);
    }

    private void initLowPass() {
        xSeriesLowPass = new LineGraphSeries<>();
        xSeriesLowPass.setColor(new Color(Color.getIntColor("#FF0000")));
        ySeriesLowPass = new LineGraphSeries<>();
        ySeriesLowPass.setColor(new Color(Color.getIntColor("#00FF00")));
        zSeriesLowPass = new LineGraphSeries<>();
        zSeriesLowPass.setColor(new Color(Color.getIntColor("#0000FF")));
        graphLowPass.getViewport().setXAxisBoundsManual(true);
        graphLowPass.getViewport().setMinX(0.0);
        graphLowPass.getViewport().setMaxX(maxDataPoints);
        graphLowPass.addSeries(xSeriesLowPass);
        graphLowPass.addSeries(ySeriesLowPass);
        graphLowPass.addSeries(zSeriesLowPass);
    }

    protected void addToGraphRaw() {
        Observable<CategoryMotionData> sensorEventObservable = createSensorEventObservable();
        sensorEventObservable.subscribe(new Observer<CategoryMotionData>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull CategoryMotionData data) {
                InnerEvent innerEvent = InnerEvent.get(1, data);
                handlerRaw.sendEvent(innerEvent);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    protected void addToGraphKalman() {
        Observable<float[]> sensorEventObservable = createSensorEventObservable()
                .map(d -> d.values);
        Observable<float[]> from3D = KalmanRx.createFrom3D(sensorEventObservable);
        Observable<float[]> lowPassFilter = KalmanRx.createLowPassFilter(from3D);
        lowPassFilter.subscribe(new Observer<float[]>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(float @NonNull [] floats) {
                InnerEvent innerEvent = InnerEvent.get(2, floats);
                handlerKalman.sendEvent(innerEvent);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    protected void addToGraphLowPass() {
        Observable<float[]> sensorEventObservable = createSensorEventObservable().map(d -> d.values);
        Observable<float[]> lowPassFilter = KalmanRx.createLowPassFilter(sensorEventObservable);
        lowPassFilter.subscribe(new Observer<float[]>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(float @NonNull [] floats) {
                InnerEvent innerEvent = InnerEvent.get(1, floats);
                handlerLowPass.sendEvent(innerEvent);
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private final EventHandler handlerRaw = new EventHandler(EventRunner.current()) {
        @Override
        protected void processEvent(InnerEvent event) {
            float[] values = ((CategoryMotionData) event.object).values;
            CategoryOrientationData.getDeviceRotationMatrix(rotationMatrix, values);
            float azimuth = ((CategoryOrientationData.getDeviceOrientation(rotationMatrix, orientation)[0] * radiansToDegrees) + 360) % 360;
            xLabelRaw.setText(String.format("%.2f", azimuth));
            yLabelRaw.setText(String.format("%.2f", 0f));
            zLabelRaw.setText(String.format("%.2f", 0f));
            graph2LastXValueRaw += 1;
            xSeriesRaw.appendData(new DataPoint(graph2LastXValueRaw, azimuth), true, maxDataPoints);
            ySeriesRaw.appendData(new DataPoint(graph2LastXValueRaw, 0), true, maxDataPoints);
            zSeriesRaw.appendData(new DataPoint(graph2LastXValueRaw, 0), true, maxDataPoints);
        }
    };

    private final EventHandler handlerLowPass = new EventHandler(EventRunner.current()) {
        @Override
        protected void processEvent(InnerEvent event) {
            float[] floats = (float[]) event.object;
            xLabelLowPass.setText(String.format("%.2f", floats[0]));
            yLabelLowPass.setText(String.format("%.2f", floats[1]));
            zLabelLowPass.setText(String.format("%.2f", floats[2]));
            graph2LastXValueLowPass += 1;
            xSeriesLowPass.appendData(new DataPoint(graph2LastXValueLowPass, floats[0]), true, maxDataPoints);
            ySeriesLowPass.appendData(new DataPoint(graph2LastXValueLowPass, floats[1]), true, maxDataPoints);
            zSeriesLowPass.appendData(new DataPoint(graph2LastXValueLowPass, floats[2]), true, maxDataPoints);
        }
    };

    private final EventHandler handlerKalman = new EventHandler(EventRunner.current()) {
        @Override
        protected void processEvent(InnerEvent event) {
            float[] floats1 = (float[]) event.object;
            xLabelKalman.setText(String.format("%.2f", floats1[0]));
            yLabelKalman.setText(String.format("%.2f", floats1[1]));
            zLabelKalman.setText(String.format("%.2f", floats1[2]));
            graph2LastXValueKalman += 1;
            xSeriesKalman.appendData(new DataPoint(graph2LastXValueKalman, floats1[0]), true, maxDataPoints);
            ySeriesKalman.appendData(new DataPoint(graph2LastXValueKalman, floats1[1]), true, maxDataPoints);
            zSeriesKalman.appendData(new DataPoint(graph2LastXValueKalman, floats1[2]), true, maxDataPoints);
        }
    };

    private Observable<CategoryMotionData> createSensorEventObservable() {
        Observable<CategoryMotionData> sensorDataObservable = Observable.create(new ObservableOnSubscribe<CategoryMotionData>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<CategoryMotionData> emitter) throws Throwable {
                MainThreadDisposable.verifyMainThread();
                orientationDataCallback = new ICategoryMotionDataCallback() {
                    @Override
                    public void onSensorDataModified(CategoryMotionData sensorData) {
                        emitter.onNext(sensorData);
                    }

                    @Override
                    public void onAccuracyDataModified(CategoryMotion categoryMotion, int i) {
                    }

                    @Override
                    public void onCommandCompleted(CategoryMotion categoryMotion) {
                    }
                };
                categoryMotionAgent = new CategoryMotionAgent();
                orientationSensor = categoryMotionAgent.getSingleSensor(CategoryMotion.SENSOR_TYPE_ACCELEROMETER);
                categoryMotionAgent.setSensorDataCallback(orientationDataCallback
                        , orientationSensor
                        , CategoryMotionAgent.SENSOR_SAMPLING_RATE_UI);
            }
        });
        return sensorDataObservable;
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        if (orientationSensor != null) {
            categoryMotionAgent.releaseSensorDataCallback(
                    orientationDataCallback, orientationSensor);
        }
    }
}
