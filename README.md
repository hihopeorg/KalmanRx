 # KalmanRx

 **本项目是基于开源项目KalmanRx进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/kibotu/KalmanRx )追踪到原项目版本**

 #### 项目介绍

 - 项目名称：KalmanRx
 - 所属系列：ohos的第三方组件适配移植
 - 功能：利用卡尔曼滤波算法对数据流进行平滑化处理，适用于过滤各种传感器输入中的噪波。
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人：hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/kibotu/KalmanRx
 - 编程语言：Java
 - 外部库依赖：无
 - 原项目基线版本：2.0.0 , sha1:7b2573030893bf1acfbb7f5e24c12f33941d01bf

 #### 演示效果

<img src="screenshot/sample.gif"/>

 #### 安装教程

 1. 编译依赖库har包kalmanrx.har。
 2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
 3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
 ```
 dependencies {
     implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
 	……
 }
 ```
 4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

 方法2.
 1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
 2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'net.kibotu.ohos:kalmanrx:1.0.0'
}
```

 #### 使用说明

 1. 利用卡尔曼滤波算法对数据流进行平滑化处理
    
 ```java
     private Observable<CategoryMotionData> createSensorEventObservable() {
            Observable<CategoryMotionData> sensorDataObservable = Observable.create(new ObservableOnSubscribe<CategoryMotionData>() {
                @Override
                public void subscribe(@NonNull ObservableEmitter<CategoryMotionData> emitter) throws Throwable {
                    MainThreadDisposable.verifyMainThread();
                    ICategoryMotionDataCallback orientationDataCallback = new ICategoryMotionDataCallback() {
                        @Override
                        public void onSensorDataModified(CategoryMotionData sensorData) {
                            emitter.onNext(sensorData);
                        }
    
                        @Override
                        public void onAccuracyDataModified(CategoryMotion categoryMotion, int i) {
                        }
    
                        @Override
                        public void onCommandCompleted(CategoryMotion categoryMotion) {
                        }
                    };
                    categoryMotionAgent = new CategoryMotionAgent();
                    orientationSensor = categoryMotionAgent.getSingleSensor(CategoryMotion.SENSOR_TYPE_ACCELEROMETER);
                    categoryMotionAgent.setSensorDataCallback(orientationDataCallback
                            , orientationSensor
                            , CategoryMotionAgent.SENSOR_SAMPLING_RATE_UI);
                }
            });
            return sensorDataObservable;
        }
     
      protected void addToGraphKalman() {
             Observable<float[]> sensorEventObservable = createSensorEventObservable()
                     .map(d -> d.values);
             Observable<float[]> from3D = KalmanRx.createFrom3D(sensorEventObservable);
             from3D.subscribe(new Observer<float[]>() {
                 @Override
                 public void onSubscribe(@NonNull Disposable d) {
     
                 }
     
                 @Override
                 public void onNext(float @NonNull [] floats) {
                    to do
                 }
     
                 @Override
                 public void onError(@NonNull Throwable e) {
     
                 }
     
                 @Override
                 public void onComplete() {
     
                 }
             });
         }
 ```

 #### 版本迭代

 - v1.0.0

  实现功能

   1. 利用卡尔曼滤波算法对数据流进行平滑化处理。

 #### 版权和许可信息
 - Copyright 2016 Jan Rabe
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
   
      http://www.apache.org/licenses/LICENSE-2.0
   
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.



